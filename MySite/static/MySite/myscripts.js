// Check browser support
//set the total ambiguous word number to zero
  if (typeof(Storage) !== "undefined")
  {
    // Store
    localStorage.setItem("totalambiguousword", 1);
  } else
  {
    document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
  }
    function refresh() {
        $.ajax({
            method: 'GET',
            url: '/data',
            success: function(result) {
                $('#text').html(result)
            },
            error: function() {
                alert('hata')
            }
        });
    }
    function managetotalambiguity(amount)
    {
      // Store
      var totalambiguousword = localStorage.getItem("totalambiguousword")
      totalambiguousword = parseInt(totalambiguousword) + amount
      console.log(totalambiguousword)
      localStorage.setItem("totalambiguousword",totalambiguousword)

    }
    //gets the selected word's definitions. Fills dropdown menu (adds buttons and definitions)
    function getWordDefinitions(wordID) {
      $.ajax({
        method: "GET",
        url : "/words/" + wordID.id,
        success : window.onload = function(result) {
          console.log($( "span" ).last().prop('id'))
          console.log(document.getElementById($( "span" ).last()))
          console.log(result)
          //take popupMenu id
          var dropdownlist = document.getElementById("popupMenu")
          //Clean selected word's definitions which are added before.
          document.getElementById('popupMenu').innerHTML = ""
          //set dropdownlist location
          newTop = wordID.offsetTop + 20
          newLeft = wordID.offsetLeft
          dropdownlist.style.top = newTop.toString() + 'px'
          dropdownlist.style.left = newLeft.toString() + 'px'
          //Create option for each definition
          radioHtml = ''
          for(var i = 0; i <= result.definitions.length; i++)
          {
            if( i < result.definitions.length )
            {
              radioHtml += '<input title="'+ result.definitions[i][0] +'" type="radio" name="' + wordID.id + '"' + 'value="' + result.definitions[i][1] + '"'
              if (result.selecteddef == result.definitions[i][0])
              {
                radioHtml += 'checked background-color="#800080;"'
              }
              radioHtml += '/>'
              radioHtml += result.definitions[i][1] +' , ' + result.definitions[i][0] + '<br />'
            }
            else if (i == result.definitions.length)
            {
              radioHtml += '<input type="radio" name="' + wordID.id + '"' + 'value="' + "none of them" + '"'
              if (result.selecteddef == 'notexist')
              {
                radioHtml += 'checked="checked" style="color:#FF0033"'
              }
              radioHtml += '/>'
              radioHtml += 'none of them' + '<br />'
            }
          }
          dropdownlist.innerHTML = radioHtml
          var storeButton = document.createElement("BUTTON")
          var buttontext = document.createTextNode("Store")
          storeButton.style.background = 'purple'
          storeButton.style.color = 'white'
          storeButton.appendChild(buttontext)

          storeButton.onclick = function()
          {
            storedbOrbrowser(wordID.id)
          }
          dropdownlist.appendChild(storeButton)
          var closeButton = document.createElement("BUTTON")
          var buttontext = document.createTextNode("Close")
          closeButton.style.background = 'grey'
          closeButton.style.color = 'white'
          closeButton.appendChild(buttontext)
          closeButton.onclick = function()
          {
            dropdownlist = document.getElementById('popupMenu')
            dropdownlist.style.display = 'none'
          };
          dropdownlist.appendChild(closeButton)
          //dropdownlist.innerHTML = result[0]
          dropdownlist.style.display = 'block'

        }
      })
    }
    //to take one word more from the current document
    function takeoneword(wordID) {
      $.ajax({
        method: "GET",
        url : "/takeoneword/" + $( "span" ).last().prop('id'),
        success : window.onload = function(result) {
          //var jsonResponse = JSON.parse(result)
          console.log(result)
          //for creating new span for the new word
          var newword = document.createElement("span")
          newword.textContent = result.word_ig
          newword.value = result.word_ig
          newword.id = result.id
          newword.className = "newword"
          a = result.id
          if (result.ambiguity == 1)
          {
            newword.style.color = '#FF0033'
            newword.load = managetotalambiguity(1)
            newword.onclick = function()
            {
              getWordDefinitions(newword)
            }
          }
          // Retrieve
          //document.getElementById("result").innerHTML = localStorage.getItem("totalambiguousword")
          text = document.getElementById("text")
          text.appendChild(newword)
        }
      })
    }
    function storedbOrbrowser(wordID)
    {
      dropdownlist = document.getElementById('popupMenu')
      selectedword = document.getElementById(wordID)
      //definitions are input elements. and their type is radio
      var radios = document.getElementsByTagName('input')
      var value
      lastwordIDinthepage = $( "span" ).last().prop('id')
      for (var i = 0; i < radios.length; i++)
      {
        if (radios[i].type == 'radio' && radios[i].checked)
        {
          // get value
          value = radios[i].value
          //The aim of the dictionary is to store annotation table data in the browser until every word is clear in the opening words(green,purple)
          // create an empty array
          var dictionary = [];
          dictionary.push({
            key:   wordID,
            value: value
          });
          $.ajax({
            method: "GET",
            url : "/storedb/" + dictionary[0].key + "/" + dictionary[0].value + "/" + lastwordIDinthepage,
            success : window.onload = function(result) {
              console.log(result)
              errorscreen = document.getElementById("result")
              errorscreen.innerHTML = result
              errorscreen.style.display = 'block'
              setTimeout(function()
              {
                $('#result').fadeOut('fast');
              }, 3000);
              if(result!="Something went wrong! Please try again...")
              {
                selectedword.style.color = '#800080'
              }
            }
          })
          console.log(dictionary)
        }
      }
      dropdownlist.style.display = 'none'
    }

  function popuphide(elmnt,clr)
  {
    elmnt.style.color = clr
    dropdownlist = document.getElementById('popupMenu')
    dropdownlist.style.display = 'none'
  }
  function userdocfunc(sentenceid)
  {
    console.log(sentenceid)
  }
