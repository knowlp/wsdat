#-*-coding: windows-1254-*-

# Word Sense Disambiguation Annotation Tool For Partially Uncovered Documents.
# Copyright (C) 2015 Kübra Cıngıllı <kubracingilli@gmail.com>
# Copyright (C) 2015 Emine İzmir <emineizmir1990@gmail.com>
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
import xml.etree.ElementTree as ET
import mysql.connector
import glob
import re

con = mysql.connector.connect(host='localhost',password='19902007',user='root',database='wordsense')
cursor =con.cursor()

add_word_treebank = ("INSERT INTO MySite_word_treebank"
                           "(word_ig, doc_id,  sentence_id, local_word_id,word, ambiguity)"
                           "VALUES(%s,%s,%s,%s,%s,%s)")
wovels_mek = "eiöü"
for name in glob.glob('devam/*.htm'):
    doc = ET.parse(name)
    for element1 in doc.findall('S'):
        doc_id1 = name
        sentence_id1 = element1.attrib['No']
        for element2 in element1.findall('W'):
            word1 = element2.text
            local_word_id1 = element2.attrib['IX']
            word_ig1 = element2.attrib['IG'].encode('utf-8')
            word_ig2 = re.sub('[ \[ \( , " \] \) 0-9]', '' , word_ig1).split('+')[0]
            if "Verb" in word_ig1:
                word_ig3 = re.compile(r'([[aeıioöuü])(?:[^[aeıioöuü])*$')
                x =re.search(word_ig3, word_ig2).group(0)
                if x[0] in wovels_mek:
                    word_ig2 = word_ig2 + "mek"
                else:
                    word_ig2 = word_ig2 + "mak"

            data_word_treebank = (word_ig2, doc_id1, sentence_id1, int(local_word_id1), word1.encode('utf-8'), 0)
            cursor.execute(add_word_treebank, data_word_treebank)
            con.commit()

cursor.close()
con.close()
