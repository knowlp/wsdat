# Word Sense Disambiguation Annotation Tool For Partially Uncovered Documents.
# Copyright (C) 2015 Kübra Cıngıllı <kubracingilli@gmail.com>
# Copyright (C) 2015 Emine İzmir <emineizmir1990@gmail.com>
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import mysql.connector

con = mysql.connector.connect(host='localhost',password='19902007',user='root',database='wordsense')
cursor = con.cursor()

update_table = '''INSERT IGNORE INTO MySite_word_to_annotate (word_ig, synset_id, definition)
    SELECT t1.word_ig, t2.synset_id, concat_ws('-', replace(t3.definition, 'null', '') , replace(t3.snote, 'null', '')) as 'definition'
    FROM MySite_word_treebank t1, MySite_literal t2, MySite_synset t3
    WHERE t1.ambiguity = 1 and t2.synset_id = t3.synset_id
    and t1.word_ig = t2.literal '''
cursor.execute(update_table, )
con.commit()
con.close()
