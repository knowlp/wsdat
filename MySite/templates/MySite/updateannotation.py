# Word Sense Disambiguation Annotation Tool For Partially Uncovered Documents.
# Copyright (C) 2015 Kübra Cıngıllı <kubracingilli@gmail.com>
# Copyright (C) 2015 Emine İzmir <emineizmir1990@gmail.com>
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from MySite.models import word_to_annotate, Literal

defin = word_to_annotate.objects.all()
a = ""
for obj1 in defin:
    literals = Literal.objects.filter(synset_id = obj1.synset_id)
    for literal in literals:
        if(obj1.definition != literal.literal and obj1.word_ig != literal.literal):
            a = a + literal.literal + " "
    if (a != ""):
        print a
        obj1.definition = obj1.definition + "(" + a + ")"
        obj1.save()
        a = ""
