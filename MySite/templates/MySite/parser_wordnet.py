#-*-coding: utf-8-*-

# Word Sense Disambiguation Annotation Tool For Partially Uncovered Documents.
# Copyright (C) 2015 Kübra Cıngıllı <kubracingilli@gmail.com>
# Copyright (C) 2015 Emine İzmir <emineizmir1990@gmail.com>
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import xml.etree.ElementTree as ET
import mysql.connector

con = mysql.connector.connect(host='localhost',password='19902007',user='root',database='wordsense')
cursor =con.cursor()
add_literal = ("INSERT INTO MySite_literal"
                           "(synset_id, literal, sense)"
                           "VALUES(%s,%s,%s)")
add_synset = ("INSERT INTO MySite_synset"
                           "(synset_id, pos, snote, definition)"
                           "VALUES(%s,%s,%s,%s)")

doc = ET.parse("wntur.xml");
for elem in doc.findall('SYNSET'):
    snote1 = None
    defin = None

    id1 = elem.find('ID').text
    for literals in elem.findall('SYNONYM/LITERAL'):
        literal = None
        sense1 = None
        literal = literals
        sense1 = literals.find('SENSE')
        data_literal = [id1.encode('utf-8'), 'null', 'null']
        try:
            if literal is not None:
                data_literal[1] = literal.text
                if sense1 is not None:
                    data_literal[2] = sense1.text
                print literal.text
                cursor.execute(add_literal,data_literal)
                con.commit()
        except (mysql.connector.errors.IntegrityError) as err:
            print ("Duplicate Entry")
    snote1 = elem.find('SNOTE')
    pos1 = elem.find('POS').text
    defin =  elem.find('DEF')
    data_synset = [id1.encode('utf-8'), pos1.encode('utf-8'),  'null', 'null']
    if snote1 is not None:
        data_synset[2] = snote1.text.encode('utf-8')
    if defin is not None:
        data_synset[3] = defin.text.encode('utf-8')
    cursor.execute(add_synset,tuple(data_synset))
    con.commit() 
    
cursor.close()
con.close()
