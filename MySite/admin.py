# Word Sense Disambiguation Annotation Tool For Partially Uncovered Documents.
# Copyright (C) 2015 Kübra Cıngıllı <kubracingilli@gmail.com>
# Copyright (C) 2015 Emine İzmir <emineizmir1990@gmail.com>
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
from django.contrib import admin
from MySite.models import Synset, Literal, Word_treebank, User_document, Annotation

class SynsetAdmin(admin.ModelAdmin):
    fields = ['synset_id','pos', 'snote', 'definition']

class LiteralAdmin(admin.ModelAdmin):
    fields = ['synset_id','literal', 'sense']

class Word_treebankAdmin(admin.ModelAdmin):
    fields = ['file_id', 'sentence_id', 'word', 'word_id', 'word_ig']

class User_documentAdmin(admin.ModelAdmin):
    fields = ['user_id','doc_id',"until_sentence_id", "until_word_id" ]

class AnnotationAdmin(admin.ModelAdmin):
    fields = ['user_id','word_id',"until_sentence_id", "until_word_id", "synset_id"]


admin.site.register(Synset, SynsetAdmin)
admin.site.register(Literal, LiteralAdmin)
admin.site.register(Word_treebank, Word_treebankAdmin)
admin.site.register(User_document, User_documentAdmin)
admin.site.register(Annotation, AnnotationAdmin)
