# Word Sense Disambiguation Annotation Tool For Partially Uncovered Documents.
# Copyright (C) 2015 Kübra Cıngıllı <kubracingilli@gmail.com>
# Copyright (C) 2015 Emine İzmir <emineizmir1990@gmail.com>
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from django.shortcuts import render, render_to_response
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from MySite.models import Synset, Word_treebank, User_document, word_to_annotate, Annotation
from django.template import RequestContext, loader
from django.db.models import Q
import random
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.context_processors import csrf
import re,json

#to store the meanings in the database.
#takes pushed definition and its word's word_id and the last word's id in the page.
login_url = '/login-page/'
def storedb(request,word_id,definition,lastword_id):
    try:
        #some definitions have "-" because of the explanation something so I have to replace them.
        definition = re.sub('-', '' , definition)
        username = request.user
        #some words have not definitions. So I put another choice for them which is "none of them"
        if definition == 'none of them':
            fromDBsynsetID = 'notexist'
        else:
            #Every definition has unique synset_id. So Ich kann finde definitions synset_id like that:
            fromDBsynsetID = word_to_annotate.objects.filter(definition=definition)[0]
            fromDBsynsetID = fromDBsynsetID.synset_id
        if len(Annotation.objects.filter(word_id = word_id)) > 0:
            fromDBuntilwordID = Annotation.objects.filter(word_id = word_id).order_by('-id')[0]
            if fromDBsynsetID != fromDBuntilwordID.synset_id:
                DBRecord = Annotation(user_id = str(username), word_id = word_id, until_sentence_id = fromDBuntilwordID.until_sentence_id, until_word_id = fromDBuntilwordID.until_word_id, synset_id = fromDBsynsetID)
                DBRecord.save()
        else:
            #burada fromDBuntilwordID treebankte, browserdan gelen lastword_id ye sahip objenin kendisi oluyor.
            fromDBuntilwordID = Word_treebank.objects.get(word_id=lastword_id)
            if User_document.objects.get(Q(user_id=username) & Q(doc_id=fromDBuntilwordID.doc_id)) is not None:
                DBRecord = Annotation(user_id = str(username), word_id = word_id, until_sentence_id = fromDBuntilwordID.sentence_id, until_word_id = fromDBuntilwordID.local_word_id, synset_id = fromDBsynsetID)
                DBRecord.save()
    except:
        return getJSONResponse("Something went wrong! Please try again...")
    return getJSONResponse(word_id)

def takeoneword(request,word_id):
    username = request.user
    word_id = int(word_id) + 1
    comingFromDB = Word_treebank.objects.get(word_id=word_id)
    try:
        UpdateUserDocData = User_document.objects.get(Q(user_id=username) & Q(doc_id=comingFromDB.doc_id))
    except:
        return getJSONResponse("you are not allowed")
    if comingFromDB.ambiguity == "1":
        ambiguity = "1"
    else:
        ambiguity = "0"
    UpdateUserDocData.until_sentence_id = comingFromDB.sentence_id
    UpdateUserDocData.until_word_id = comingFromDB.local_word_id
    UpdateUserDocData.save()
    if comingFromDB.word is " .":
        comingFromDB.word = comingFromDB.word.replace(" ","")
    if "_" in comingFromDB.word:
        comingFromDB.word = comingFromDB.word.replace("_", " ")
    word = {"id" : word_id, "word_ig" : comingFromDB.word, "ambiguity": ambiguity}
    return getJSONResponse(word)

@login_required(login_url=login_url)
def getWordInformation(request, word_id):
    comingFromDBs = Word_treebank.objects.get(word_id=word_id)
    comingFromDBs = word_to_annotate.objects.filter(word_ig=comingFromDBs.word_ig)
    if len(Annotation.objects.filter(word_id = word_id)) > 0:
        selecteddef = Annotation.objects.filter(word_id = word_id).order_by('-id')[0]
        selecteddef = selecteddef.synset_id
    else:
        selecteddef = "notselectedyet"
    definitions = ()
    for comingFromDB in comingFromDBs:
        if comingFromDB.definition != "":
            tup1=(comingFromDB.synset_id,comingFromDB.definition)
            definitions = (tup1,)+ definitions
    definition = {
        "definitions": definitions,
        "selecteddef": selecteddef
    }
    return getJSONResponse(definition)

def getJSONResponse(data):
    return HttpResponse(json.dumps(data), content_type="application/json")

def login_page(request):
    c = {}
    c.update(csrf(request))
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_authenticated:
                login(request, user)
                # Redirect to a success page.
                return redirect('/MySite/')
            else:
            # Return a 'disabled account' error message
                return HttpResponse('<b>disabled account</b>')
        else:
        # Return an 'invalid login' error message.
            return redirect('/login-page/')
        return HttpResponse('<b>User:</b> %s <b>Password:</b> %s'%(username,password))
    else:
        return render_to_response('MySite/get_post.html',c)

def logout_view(request):
    logout(request)
    return redirect(login_url)

@login_required(login_url=login_url)
def assigned_document(request):
    username = request.user
    docs = User_document.objects.order_by('doc_id').filter(user_id=username)
    treebankdocs = Word_treebank.objects.all()
    numberofwords = ()

    for doc in docs:
        for treebankdoc in treebankdocs:
            pass
        until_sentence_id = re.split('-',doc.until_sentence_id)
        count = re.split('-',treebankdoc.sentence_id)
        if len(until_sentence_id) > 1 and len(count) > 1:
            count = count[1]
            until_sentence_id = until_sentence_id[1]
        else:
            count = count[0]
            until_sentence_id = until_sentence_id[0]
        count = round(float((100*float(until_sentence_id))/float(count)),2)
        count = str(count) + "%"
        tup1=(doc,count)
        numberofwords=(tup1,)+numberofwords

    data = {
        'username' : username,
        'numberofwords' : numberofwords,
    }
    return render(request, 'MySite/document.html',data)

@login_required(login_url=login_url)
def page_html(request,content_id):
    selecteddoc = User_document.objects.filter(id = content_id)
    pages = Word_treebank.objects.all()
    username = request.user
    page = list()
    #count = 0
    #ambiguityflag for automaticly open last ambiguous word
    #ambiguityflag = 0
    for object1 in pages:
        for object2 in selecteddoc:
            doc_sentence_id = re.split('-',object1.sentence_id )
            until_sentence_id = re.split('-',object2.until_sentence_id )
            if object1.doc_id == object2.doc_id:
                if "_" in object1.word:
                    object1.word = object1.word.replace("_", " ")
                #if current sentence greater than the user_document table's until_sentence_id
                if len(until_sentence_id) > 1 and len(doc_sentence_id) > 1:
                    doc_sentence_id = doc_sentence_id[1]
                    until_sentence_id = until_sentence_id[1]
                else:
                    doc_sentence_id = doc_sentence_id[0]
                    until_sentence_id = until_sentence_id[0]

                if float(until_sentence_id) > float(doc_sentence_id):
                    #count = count + 1
                    page.append(object1)
                #if current sentence equal to the user_document table's until_sentence_id
                elif float(until_sentence_id) == float(doc_sentence_id) or float(until_sentence_id) == 0:
                    if object1.local_word_id <= object2.until_word_id:
                        #count = count + 1
                        page.append(object1)
                        userdoc = object2
                    # elif object1.local_word_id > object2.until_word_id and ambiguityflag == 0:
                    #     count = count + 1
                    #     page.append(object1)
                    #     userdoc = object2
                    #     if object1.ambiguity is '1':
                    #         ambiguityflag = 1
                try:
                    if len(Annotation.objects.all().filter(word_id = object1.word_id)) < 1 and object1.ambiguity == '1':
                        object1.ambiguity = "10"
                except:
                    pass
    data={
                      'page'             : page,
                      'username'         : username,
                      'userdoc'          : userdoc,
                      }
    return render(request, 'MySite/user_document.html',data)
