# Word Sense Disambiguation Annotation Tool For Partially Uncovered Documents.
# Copyright (C) 2015 Kübra Cıngıllı <kubracingilli@gmail.com>
# Copyright (C) 2015 Emine İzmir <emineizmir1990@gmail.com>
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
from django.db import models

class Synset(models.Model):

    synset_id = models.CharField(primary_key=True, max_length=50)
    pos = models.CharField(max_length=1)
    snote = models.CharField(max_length=500,null=True)
    definition = models.CharField(max_length=500,null=True)

    def __str__(self):              # __unicode__ on Python 2
        return self.synset_id

class Literal(models.Model):

    id = models.AutoField(primary_key=True)
    literal = models.CharField(max_length=50)
    sense = models.CharField(max_length=1)
    synset_id = models.CharField(max_length=50)

    class Meta:
        unique_together = (('literal','sense'),)

    def __str__(self):
        return self.literal

class Word_treebank(models.Model):

    word_id = models.AutoField(primary_key=True)
    word_ig = models.CharField(max_length=50)
    doc_id = models.CharField(max_length=30)
    sentence_id = models.CharField(max_length=15)
    local_word_id = models.IntegerField()
    word = models.CharField(max_length=50)
    ambiguity = models.BinaryField(default=0)

    def __str__(self):
        return self.word

class word_to_annotate(models.Model):
    id = models.AutoField(primary_key=True)
    word_ig = models.CharField(max_length=50)
    synset_id = models.CharField(max_length=50)
    definition = models.CharField(max_length=500)
    class Meta:
        unique_together = (('word_ig','synset_id'),)
    def __str__(self):
        return self.word_ig

class User_document(models.Model):
    doc_id = models.CharField(max_length=30)
    user_id = models.CharField(max_length=20, default = None)
    until_sentence_id = models.CharField(max_length=15, default = None)
    until_word_id = models.IntegerField()
    class Meta:
        unique_together = (('doc_id','user_id'),)

    def __str__(self):
        return self.doc_id

class Annotation(models.Model):
    user_id = models.CharField(max_length=20)
    word_id = models.IntegerField()
    until_sentence_id = models.CharField(max_length=15)
    until_word_id = models.IntegerField()
    synset_id = models.CharField(max_length=50)

    def __str__(self):
        return self.word_id
