# Word Sense Disambiguation Annotation Tool For Partially Uncovered Documents.
# Copyright (C) 2015 Kübra Cıngıllı <kubracingilli@gmail.com>
# Copyright (C) 2015 Emine İzmir <emineizmir1990@gmail.com>
# Copyright (C) 2015 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from django.conf.urls import patterns, include, url

from django.contrib import admin
from MySite import views
import MySite.views
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ExampleProject1.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^MySite/',views.assigned_document, name='assigned_document'),
    #url(r'^process/',views.index, name='index'),
    url(r'^page/(?P<content_id>[0-9]+)/$',views.page_html, name='page_html'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^words/(?P<word_id>[0-9]+)/$',MySite.views.getWordInformation),
    url(r'^takeoneword/(?P<word_id>[0-9]+)/$',MySite.views.takeoneword),
    url(r'^login-page/',MySite.views.login_page),
    url(r'^logout/',MySite.views.logout_view),
    url(r'^storedb/(?P<word_id>[0-9]+)/(?P<definition>[\s\S]*)/(?P<lastword_id>[0-9]+)/$',MySite.views.storedb),
    #url(r'^data/$', views.take_one_word, name='take_one_word'),
    url(r'^$', MySite.views.login_page),
)
